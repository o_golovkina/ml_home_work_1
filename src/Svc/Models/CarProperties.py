from pydantic import BaseModel
from typing import List


class CarProperty(BaseModel):
    name: str
    year: int
    selling_price: int
    km_driven: int
    fuel: str
    seller_type: str
    transmission: str
    owner: str
    mileage: str
    engine: str
    max_power: str
    torque: str
    seats: float


class CarsProperties(BaseModel):
    objects: List[CarProperty]
