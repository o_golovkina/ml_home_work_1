import re

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random
import numpy as np

from sklearn.metrics import r2_score
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from src.Svc.TextPreprocessor import delete_symbols_from_cells_by_columns
from src.Svc.TorquePreprocessor import split_torque_column

from sklearn.linear_model import LinearRegression, Ridge


# Можно было бы сделать абстрактный класс и определить интерфейс и дальше реализовать для разных моделей, но не в этот раз...
# Ну и SOLID ещё отлетает...
class Predictor:
    __conv_columns = ['mileage', 'engine', 'max_power']

    def __init__(self, dataset: pd.DataFrame):
        random.seed(42)
        np.random.seed(42)

        dataset.drop_duplicates(keep='first', inplace=True)

        self.__X_train = self.__prepare_data(dataset)
        self.__Y_train = dataset['selling_price']

        self.__Model = self.__train_model(self.__X_train, self.__Y_train)

    def __prepare_data(self, dataset: pd.DataFrame):
        df_clone = dataset.copy()

        if 'name' in df_clone and 'selling_price' in df_clone:
            df_clone.drop(columns=['name', 'selling_price'], axis=1, inplace=True)

        df_clone = delete_symbols_from_cells_by_columns(df_clone, self.__conv_columns)
        df_clone.reset_index(drop=True, inplace=True)
        df_clone[self.__conv_columns] = df_clone[self.__conv_columns].astype(float)
        df_clone = split_torque_column(df_clone)

        if df_clone.isnull().values.any():
            df_clone.fillna(df_clone.median(), inplace=True)

        df_clone['seats'] = df_clone['seats'].astype(int)

        return df_clone.select_dtypes(include=np.number)

    def __train_model(self, props: pd.DataFrame, target_val: pd.DataFrame):
        ridge_model = Ridge()
        pipe = Pipeline(steps=[("ridge", ridge_model)])
        solver = ["auto", "svd", "cholesky", "lsqr", "sparse_cg", "sag", "saga"]

        parameters = dict(ridge__solver=solver)

        gridCV = GridSearchCV(pipe, parameters)
        gridCV.fit(props, target_val)

        gridCV.best_estimator_.fit(props, target_val)

        return gridCV.best_estimator_

    def Predict(self, df: pd.DataFrame):
        prep = self.__prepare_data(df)
        return self.__Model.predict(prep)

# %%
