import re

import numpy as np
import pandas as pd


def delete_units(value):
    if pd.isna(value):
        return np.nan

    match_res = re.match(r"\d+|\d+\.\d+", value)

    if match_res is not None:
        return match_res.group(0).strip()
    else:
        return np.nan


def delete_symbols_from_cells_by_columns(df: pd.DataFrame, columns: list):
    df_clone = pd.DataFrame.copy(df)

    for column in columns:
        df_clone[column] = df_clone[column].apply(delete_units)

    return df_clone