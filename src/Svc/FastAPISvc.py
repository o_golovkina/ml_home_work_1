from http.client import HTTPException
from typing import List
from io import StringIO

from fastapi import FastAPI, File, UploadFile
from pydantic import BaseModel
from starlette import status
import pandas as pd
from starlette.responses import StreamingResponse

from src.Svc.Models.CarProperties import CarProperty
import uvicorn
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

import uvicorn
from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import HTMLResponse

from src.Svc.PredictionSvc import Predictor

from fastapi.responses import FileResponse

df_train = pd.read_csv('https://raw.githubusercontent.com/Murcha1990/MLDS_ML_2022/main/Hometasks/HT1/cars_train.csv')
predictor = Predictor(df_train)

app = FastAPI()

@app.get("/")
async def main():
    content = """
        <body>
        <center>
            <form action="/submit_csv_file/" enctype="multipart/form-data" method="post">
                <p>Выберите csv файл для предсказания стоимости автомобиля:</p>
                <input name="file" type="file" accept=".csv">
                <input type="submit">
            </form>
            <form action="/submit_json_file/" enctype="multipart/form-data" method="post">
                <p>Выберите json файл для предсказания стоимости автомобиля:</p>
                <input name="file" type="file" accept=".json">
                <input type="submit">
            </form>
        </center>
        </body>
    """
    return HTMLResponse(content=content)

@app.post("/submit_csv_file/")
async def submit_csv_file(file: UploadFile):
    if file is None or file.filename == '':
        raise HTTPException(status_code=404, detail="Файл не был выбран")
    else:
        df = pd.read_csv(StringIO(str(file.file.read(), 'utf-8')), encoding='utf-8', delimiter=',')
        df = get_predict(df)

        stream = StringIO()

        df.to_csv(stream, index=False)

        response = StreamingResponse(iter([stream.getvalue()]),
                                     media_type="text/csv")

        response.headers["Content-Disposition"] = "attachment; filename=pred_result.csv"

        return response

@app.post("/submit_json_file/")
async def submit_json_file(file: UploadFile):
    if file is None or file.filename == '':
        raise HTTPException(status_code=404, detail="Файл не был выбран")
    else:
        df = pd.read_json(StringIO(str(file.file.read(), 'utf-8')), encoding='utf-8')
        res = get_predict(df)

        if len(res) > 1:
            stream = StringIO()

            res.to_json(stream, orient="records")

            response = StreamingResponse(iter([stream.getvalue()]),
                                         media_type="text/json")

            response.headers["Content-Disposition"] = "attachment; filename=pred_result.json"

            return response
        elif len(df) == 1:
            html_content = f"""
            <html>
                <head>
                    <title>Результат предсказания стоимости автомобиля</title>
                </head>
                <body>
                   <p>Предсказанная стоимость автомобиля: {list(res['pred_selling_price'])[0]}</p>
                   <p>
                    Исходные характеристики:
                    <br>
                    {df.to_dict('records')}
                   </p>
                </body>
            </html>
            """
            return HTMLResponse(content=html_content, status_code=200)

@app.post("/predict_item")
async def predict_item(item: CarProperty):
    dict_items = item.dict()
    return get_predict(pd.DataFrame([dict_items])).to_dict('records')


@app.post("/predict_items/")
async def predict_items(items: List[CarProperty]):
    dict_items = [x.dict() for x in items]
    return get_predict(pd.DataFrame(dict_items)).to_dict('records')

def get_predict(df: pd.DataFrame) -> pd.DataFrame:
    pred_column = predictor.Predict(df)
    df['pred_selling_price'] = list(pred_column)

    return df

uvicorn.run(app, host="127.0.0.1", port=8000)
