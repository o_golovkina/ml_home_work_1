import numpy as np
import pandas as pd

from src.Svc.TextPreprocessor import delete_units


def get_torque_in_nm(value):
    if pd.isna(value):
        return np.nan

    coefficient = 1

    if "kgm" in value:
        coefficient = 9.80665

    val_without_units = delete_units(value)

    if len(val_without_units) == 0:
        val_without_units

    return str(float(val_without_units) * coefficient)


def get_max_rpm(value):
    if pd.isna(value):
        return np.nan

    vals = [float(delete_units(x)) for x in value.strip().split('-')]
    return sum(vals) / len(vals)


#это придется допиливать, когда появятся новые паттерны записи, но в целом, это основа для дальнейшего извлечения инфы из значения
def primary_formating_text(value: str):
    value = str(value)
    value = value.strip().lower()
    value = value.replace(',', '.').replace('at', '@').strip().replace(' ', '').replace('+/-', '-').strip()

    formatting_kgm_rpm = '(kgm@rpm)'
    formattingNmRpm = '(nm@rpm)'

    if formatting_kgm_rpm in value:
        value = value.replace(formatting_kgm_rpm, '')
        splitted_value = value.split('@')
        value = f"{splitted_value[0]}kgm@{splitted_value[-1]}rpm"

    if formattingNmRpm in value:
        value = value.replace(formattingNmRpm, '')
        splitted_value = value.split('@')
        value = f"{splitted_value[0]}nm@{splitted_value[-1]}rpm"

    return value


def convert_to_torque_and_max_torque_rpm(value):
    if pd.isna(value):
        return [np.nan, np.nan]

    value = primary_formating_text(value)
    torques = value.split('@')

    torque = get_torque_in_nm(torques[0])
    max_torque = get_max_rpm(torques[-1])

    return [torque, max_torque]


def split_torque_column(df: pd.DataFrame):
    df_clone = pd.DataFrame.copy(df)
    original_values = df_clone['torque'].values

    converted_values = [convert_to_torque_and_max_torque_rpm(x) for x in original_values]

    df_clone['torque'] = [x[0] for x in converted_values]
    df_clone['torque'] = df_clone['torque'].astype(float)

    df_clone.insert(11, 'max_torque', [x[-1] for x in converted_values])
    df_clone['max_torque'] = df_clone['max_torque'].astype(float)

    return df_clone